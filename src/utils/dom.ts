import Card from "../components/card";
import { filmCategories, IMovie } from "../types/common";

export const savePageAndCategory = ({ page, category }: { page?: number, category?: filmCategories }) => {
  if (page) {
    document.body.dataset.page = String(page);
  }
  if (category) {
    document.body.dataset.category = category;
  }
}

export const getPageAndCategory = (): { page: number, category: filmCategories } => {
  return {
    page: parseInt(document.body.dataset.page!),
    category: document.body.dataset.category as filmCategories,
  }
}

export const clearMovieList = (): void => {
  const filmContainer = document.querySelector("#film-container");

  if (filmContainer) {
    filmContainer.innerHTML = '';
  }
}

export const updateMovieList = (movies: IMovie[]): void => {
  const filmContainer = document.querySelector("#film-container");

  movies.map((movie) => {
    const card = new Card(movie);
    card.render(filmContainer!)
  })
}

export const updateRandomMovie = (movies: IMovie[]): void => {
  const randomNumber = Math.floor(Math.random() * (movies.length - 2) + 1);
  const movie = movies[randomNumber];
  const randomMovieContainer: HTMLElement | null = document.querySelector("#random-movie");
  if (randomMovieContainer) {
    randomMovieContainer.style.backgroundImage = `url('https://image.tmdb.org/t/p/original${movie.backdrop_path}')`;
    randomMovieContainer.style.backgroundSize = 'cover';
  }
  const randomMovieName: HTMLElement | null = document.querySelector("#random-movie-name");
  if (randomMovieName) {
    randomMovieName.innerText = movie.title
  }
  const randomMovieDescription: HTMLElement | null = document.querySelector("#random-movie-description");
  if (randomMovieDescription) {
    randomMovieDescription.innerText = movie.overview
  }
}