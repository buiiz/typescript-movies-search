import { fetchMoviesByCategory, fetchMoviesSearch } from "../services/apiService";
import { filmCategories } from "../types/common";
import { clearMovieList, updateMovieList, savePageAndCategory, getPageAndCategory, updateRandomMovie } from "./dom";

export const handleCategoryChange = (): void => {
  const categoryButtons = document.querySelectorAll("#button-wrapper > input[type='radio']")

  categoryButtons.forEach((button) => button.addEventListener('change', async (e: Event): Promise<void> => {
    const category = (e.target as HTMLInputElement).id;
    savePageAndCategory({ page: 1, category: category as filmCategories })
    const movies = await fetchMoviesByCategory(category as filmCategories);
    await clearMovieList();
    await updateMovieList(movies)
  }))
}

export const handlePageLoad = (): void => {
  document.addEventListener("DOMContentLoaded", async () => {
    const movies = await fetchMoviesByCategory('popular');
    await updateRandomMovie(movies)
    await updateMovieList(movies)
  })
}

export const handleLoadNextPage = (): void => {
  const loadMoreButton = document.querySelector("#load-more")

  loadMoreButton?.addEventListener('click', async () => {
    const { page, category } = getPageAndCategory()
    const newPage = page + 1;
    savePageAndCategory({ page: newPage })
    const movies = await fetchMoviesByCategory(category, newPage);
    await updateMovieList(movies)
  })
}

export const handleSearch = (): void => {
  const searchButton = document.querySelector("#submit")
  const searchInput: HTMLInputElement | null = document.querySelector("#search")

  searchButton?.addEventListener('click', async () => {
    const query = searchInput?.value;
    if (query) {
      const movies = await fetchMoviesSearch(query)
      await clearMovieList();
      await updateMovieList(movies)
    }
  })
}
