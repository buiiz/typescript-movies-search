import { IMovie, ResponseType } from "../types/common";

export const mapResponse = (response: ResponseType): IMovie[] => {
  return response.results.map((movie) => {
    const { id, title, poster_path, release_date, overview, backdrop_path } = movie;
    return { id, title, poster_path, release_date, overview, backdrop_path }
  })
}