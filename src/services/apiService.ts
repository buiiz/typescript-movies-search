import { filmCategories, IMovie } from "../types/common";
import { mapResponse } from "./movieMapper";

const API_KEY = process.env.TMDB_API_KEY;
const API_READ_ACCESS_TOKEN = process.env.TMDB_API_READ_ACCESS_TOKEN;
const BASE_URL = `https://api.themoviedb.org/3`

export const fetchCommon = async (url: string): Promise<Response> => {
  return await fetch(url, {
    headers: {
      'Content-Type': 'application/json;charset=utf-8',
      'Authorization': `Bearer ${API_READ_ACCESS_TOKEN}`,
    }
  });
}

export const fetchMoviesByCategory = async (category: filmCategories, page = 1): Promise<IMovie[]> => {
  try {
    const url = `${BASE_URL}/movie/${category}?api_key=${API_KEY}&page=${page}&include_adult=true`;
    const res = await fetchCommon(url);
    const data = await res.json()
    return mapResponse(data);
  } catch (error) {
    throw new Error("fetchMoviesByCategory error");
  }
}

export const fetchMoviesSearch = async (query: string, page = 1): Promise<IMovie[]> => {
  try {
    const url = `${BASE_URL}/search/movie?api_key=${API_KEY}&query=${query}&page=${page}&include_adult=true`;
    const res = await fetchCommon(url);
    const data = await res.json()
    return mapResponse(data);
  } catch (error) {
    throw new Error("fetchMoviesSearch error");
  }
}

