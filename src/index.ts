import { handleCategoryChange, handleLoadNextPage, handlePageLoad, handleSearch } from './utils/handlers'

export async function render(): Promise<void> {
  handlePageLoad();
  handleCategoryChange();
  handleLoadNextPage();
  handleSearch();
}
