import { IMovie } from '../types/common';
import Component from './component'
import HeartButton from './heartButton';

export default class Card extends Component {
  readonly element: HTMLElement;

  public onClick: (e?: MouseEvent) => void = () => {
    console.log('The click handler has not been defined in a Card component');
  }

  constructor(movie: IMovie) {
    super()
    this.element = document.createElement('div')
    this.element.classList.add('col-lg-3', 'col-md-4', 'col-12', 'p-2');

    const card = document.createElement('div')
    card.classList.add('card', 'shadow-sm');
    this.element.appendChild(card)

    const img = document.createElement('img')
    img.src = `https://image.tmdb.org/t/p/w500${movie.poster_path}`
    card.appendChild(img)

    const heartButton = new HeartButton(false);
    heartButton.render(card)

    const cardBody = document.createElement('div')
    cardBody.classList.add('card-body');
    card.appendChild(cardBody)

    const cardText = document.createElement('p')
    cardText.classList.add('card-text', 'truncate');
    cardText.innerText = movie.overview
    cardBody.appendChild(cardText);

    const cardTime = document.createElement('div')
    cardTime.classList.add('d-flex', 'justify-content-between', 'align-items-center');
    cardBody.appendChild(cardTime)

    const time = document.createElement('small')
    time.classList.add('text-muted');
    time.innerText = movie.release_date
    cardTime.appendChild(time)

    this.element.addEventListener('click', e => {
      e.preventDefault()
      e.stopPropagation()
      this.onClick()
    })
  }
}
