import { SVGInHTMLElement } from '../types/common'
import Component from './component'

export default class heartButton extends Component {
  readonly element: SVGInHTMLElement

  public onClick: () => void = () => {
    console.log('The click handler has not been defined in a Button component.');
  }

  constructor(isActive: boolean) {
    super()
    this.element = document.createElementNS('http://www.w3.org/2000/svg', 'svg') as SVGInHTMLElement
    this.element.classList.add('heart-button', 'position-absolute', 'p-2', 'bi', 'bi-heart-fill');

    this.element.setAttribute('xmlns', 'http://www.w3.org/2000/svg');
    this.element.setAttribute('stroke', 'red');
    this.element.setAttribute('fill', isActive ? 'red' : "#ff000078");
    this.element.setAttribute('width', '50');
    this.element.setAttribute('height', '50');
    this.element.setAttribute('viewBox', '0 -2 18 22');

    const path = document.createElementNS('http://www.w3.org/2000/svg', 'path')
    path.setAttribute('fill-rule', 'evenodd');
    path.setAttribute('d', 'M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z');
    this.element.appendChild(path)

    path.addEventListener('click', e => {
      e.preventDefault()
      e.stopPropagation()
      this.onClick()
    })
  }
}
